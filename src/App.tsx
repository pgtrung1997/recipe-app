import React, { useState } from "react";
import "./App.css";
import "bootstrap/dist/css/bootstrap.min.css";
import Navbar from "react-bootstrap/Navbar";
import { Nav } from "react-bootstrap";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { LinkContainer } from "react-router-bootstrap";
import Recipes from "./components/Recipes";
import ShoppingList from "./components/ShoppingList";
import { Values } from "./components/IRecipe";

function App() {
  const [info, setInfo] = useState<any[]>([]);
  const [currentInfo, setCurrentInfo] = useState<any>({});

  const createRecipe = (value: Values) => {
    setInfo([...info, value]);
  };

  const editRecipe = (value: Values, index: number) => {
    let update = info.map((v: Values, i: number) => {
      if (i === index) {
        v = value;
      }
      return v;
    });
    setInfo(update);
  };

  const deleteRecipe = (index: number) => {
    info.splice(index, 1);
    setInfo([...info]);
  };

  const addToShopping = (index: number) => {
    setCurrentInfo(info[index]);
  };

  const updateIngredient = (value: Values) => {
    setCurrentInfo(value);
  };

  return (
    <div className="App">
      <Router>
        <Navbar bg="light" expand="lg" className="py-0">
          <Navbar.Brand>Recipe Book</Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav variant="pills" className="mr-auto">
              <LinkContainer to="/recipes">
                <Nav.Link className="rounded-0 py-3">Recipes</Nav.Link>
              </LinkContainer>
              <LinkContainer to="/shopping-list">
                <Nav.Link className="rounded-0 py-3">Shopping List</Nav.Link>
              </LinkContainer>
            </Nav>
          </Navbar.Collapse>
        </Navbar>

        <Switch>
          <Route path="/recipes">
            <Recipes
              onSub={createRecipe}
              onEdit={editRecipe}
              onDelete={deleteRecipe}
              onAdd={addToShopping}
              info={info}
            />
          </Route>
          <Route path="/shopping-list">
            <ShoppingList onAdd={updateIngredient} currentInfo={currentInfo} />
          </Route>
        </Switch>
      </Router>
    </div>
  );
}

export default App;
