import React, { useState } from "react";
import { Button } from "react-bootstrap";
import { Container, Row, Col, Dropdown, DropdownButton } from "react-bootstrap";
import { LinkContainer } from "react-router-bootstrap";
import FormRecipe from "./FormRecipe";
import EditRecipe from "./EditRecipe";
import style from "./Recipes.module.css";
import { Values, Ing } from "./IRecipe";
import {
  BrowserRouter as Router,
  Switch,
  useRouteMatch,
  Route,
  useHistory,
} from "react-router-dom";

interface IRecipeProps {
  onEdit: (value: Values, index: number) => void;
  onSub: (value: Values) => void;
  onDelete: (index: number) => void;
  onAdd: (index: number) => void;
  info: Values[];
}

const Recipes = (props: IRecipeProps) => {
  let { path, url } = useRouteMatch();
  const { onSub, onEdit, onDelete, onAdd, info } = props;
  const [select, setSelect] = useState<string>("");
  const [display, setDisplay] = useState<boolean>(false);
  const history = useHistory();
  let convert = Number(select);

  const onSelect = (e: React.MouseEvent<HTMLElement>) => {
    setSelect((e.target as any).id);
    setDisplay(true);
    history.push("/recipes");
  };

  const toShoppingList = () => {
    history.push("/shopping-list");
    onAdd(convert);
  };

  const deleteHandle = () => {
    onDelete(convert);
    setDisplay(false);
  };

  return (
    <Container className="text-left mt-4">
      <Row>
        <Col>
          <LinkContainer to={`${url}/form`}>
            <Button
              variant="success"
              onClick={() => {
                setDisplay(false);
                setSelect("");
              }}
            >
              New Recipe
            </Button>
          </LinkContainer>
          <hr />
          {info.map((v: Values, i: number) => {
            return (
              <div
                key={i}
                id={`${i}`}
                className={`d-flex justify-content-between align-items-center border border-info rounded px-3 py-2 my-3 ${
                  style["item"]
                } ${select === i.toString() ? style["active"] : ""}`}
                onClick={onSelect}
              >
                <div className="item-left">
                  <h5>{v.name}</h5>
                  <p>{v.desc}</p>
                </div>

                <img src={v.image} alt={v.name} className="w-25 h-100" />
              </div>
            );
          })}
        </Col>

        <Col>
          <div className={`detail ${display ? "" : style["deactive"]}`}>
            <img
              src={info[convert]?.image}
              alt={info[convert]?.name}
              className="w-50"
            />
            <h2>{info[convert]?.name}</h2>

            <DropdownButton id="dropdown-basic-button" title="Manage Recipe">
              <Dropdown.Item onClick={toShoppingList}>
                To Shopping List
              </Dropdown.Item>

              <LinkContainer to={`${url}/edit`}>
                <Dropdown.Item
                  onClick={() => {
                    setDisplay(false);
                  }}
                >
                  Edit Recipe
                </Dropdown.Item>
              </LinkContainer>
              <Dropdown.Item onClick={deleteHandle}>
                Delete Recipe
              </Dropdown.Item>
            </DropdownButton>

            <p>{info[convert]?.desc}</p>
            {info[convert]?.ingredient.map((v: Ing, i: number) => {
              return (
                <div key={i} className="border rounded px-3 py-2">
                  {v.item} - {v.quantity}
                </div>
              );
            })}
          </div>

          <div className="form">
            <Switch>
              <Route path={`${path}/form`}>
                <FormRecipe onSub={onSub} />
              </Route>
              <Route path={`${path}/edit`}>
                <EditRecipe
                  data={info[convert]}
                  onEdit={onEdit}
                  index={convert}
                />
              </Route>
            </Switch>
          </div>
        </Col>
      </Row>
    </Container>
  );
};

export default Recipes;
