import React from "react";
import { Formik, Form, Field, FieldArray } from "formik";
import { Button } from "react-bootstrap";
import { useHistory } from "react-router";
import { Values } from "./IRecipe";

interface IEditRecipeProps {
  index: number;
  data: Values;
  onEdit: (value: Values, index: number) => void;
}

const EditRecipe = (props: IEditRecipeProps) => {
  const history = useHistory();
  const { data, onEdit, index } = props;
  const initialValues = {
    name: data.name,
    image: data.image,
    desc: data.desc,
    ingredient: data.ingredient,
  };

  const onCancel = () => {
    history.push("/recipes");
  };

  return (
    <>
      <Formik
        initialValues={initialValues}
        onSubmit={(values: Values) => {
          onEdit(values, index);
        }}
      >
        {({ values }) => (
          <Form className="d-flex flex-column">
            <div className="button-group">
              <Button
                type="submit"
                variant="success"
                className="mr-1"
                disabled={!values.name || !values.image || !values.desc}
              >
                Update
              </Button>
              <Button
                type="button"
                variant="danger"
                className="ml-1"
                onClick={onCancel}
              >
                Cancel
              </Button>
            </div>

            <div className="form-group mt-3">
              <label htmlFor="name">Name</label>
              <Field
                id="name"
                name="name"
                type="text"
                className="form-control"
              ></Field>
            </div>

            <div className="form-group">
              <label htmlFor="image">Image URL</label>
              <Field
                id="image"
                name="image"
                type="url"
                className="form-control"
              ></Field>
              <img src={values.image} alt={values.name} className="w-50 mt-3" />
            </div>

            <div className="form-group">
              <label htmlFor="desc">Description</label>
              <Field
                id="desc"
                name="desc"
                as="textarea"
                className="form-control"
              ></Field>
            </div>

            <div className="extras mt-4">
              <FieldArray name="ingredient">
                {(fieldArrayProps) => {
                  const { push, remove } = fieldArrayProps;
                  return (
                    <div className="ingredient-list">
                      {values.ingredient.map((ingredient, index) => (
                        <div
                          key={index}
                          className="d-flex justify-content-between mb-3 form-group"
                        >
                          <Field
                            type="text"
                            name={`ingredient[${index}].item`}
                            className="w-50 form-control"
                          />
                          <Field
                            type="number"
                            min="1"
                            name={`ingredient[${index}].quantity`}
                            className="w-25 form-control"
                          />
                          <Button
                            type="button"
                            variant="danger"
                            onClick={() => remove(index)}
                          >
                            x
                          </Button>
                        </div>
                      ))}
                      <hr />
                      <Button
                        type="button"
                        variant="success"
                        onClick={() => push({ item: "", quantity: "" })}
                      >
                        Add Ingredient
                      </Button>
                    </div>
                  );
                }}
              </FieldArray>
            </div>
          </Form>
        )}
      </Formik>
    </>
  );
};

export default EditRecipe;
