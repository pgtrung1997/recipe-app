import React from "react";
import { Formik, Form, Field, FieldArray } from "formik";
import { Button } from "react-bootstrap";
import { Values } from "./IRecipe";

interface IShoppingListProps {
  onAdd: (value: Values) => void;
  currentInfo: Values;
}

const ShoppingList = (props: IShoppingListProps) => {
  const { onAdd, currentInfo } = props;
  const initialValues = {
    ingredient: currentInfo.ingredient,
  };

  return (
    <>
      <Formik
        initialValues={initialValues}
        onSubmit={(values) => {
          currentInfo.ingredient = values.ingredient;
          onAdd(currentInfo);
        }}
      >
        {({ values }) => (
          <Form className="d-flex flex-column w-25 my-4">
            <FieldArray name="ingredient">
              {(fieldArrayProps) => {
                const { push, remove } = fieldArrayProps;
                return (
                  <>
                    <div className="mb-4">
                      <Button type="submit" variant="success" className="mx-1">
                        Save
                      </Button>
                      <Button
                        type="button"
                        variant="primary"
                        onClick={() => push({ item: "", quantity: "" })}
                        className="mx-1"
                      >
                        Add
                      </Button>
                    </div>
                    <div className="ingredient-list">
                      {values.ingredient?.map((ingredient, index) => (
                        <div
                          key={index}
                          className="d-flex justify-content-between mb-3 form-group"
                        >
                          <Field
                            type="text"
                            name={`ingredient[${index}].item`}
                            className="w-50 form-control"
                          />
                          <Field
                            type="number"
                            min="1"
                            name={`ingredient[${index}].quantity`}
                            className="w-25 form-control"
                          />
                          <Button
                            type="button"
                            variant="danger"
                            onClick={() => remove(index)}
                          >
                            x
                          </Button>
                        </div>
                      ))}
                    </div>
                  </>
                );
              }}
            </FieldArray>
          </Form>
        )}
      </Formik>
    </>
  );
};

export default ShoppingList;
