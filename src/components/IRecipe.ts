export interface Values {
    name: string;
    image: string;
    desc: string;
    ingredient: Ing[];
  }
  
export interface Ing {
    item: string;
    quantity: string;
  }